package com.twuc.webApp.web;

import org.springframework.hateoas.Link;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("users")
public class UserController {

    // TODO:
    //
    // 请实现如下的 API。你可能需要为方法添加额外的参数或者 annotation：
    //
    // | Key            | Description                                                           |
    // |----------------|-----------------------------------------------------------------------|
    // | URI            | /users/<id>                                                           |
    // | HTTP Method    | GET                                                                   |
    // | Content-Type   | application/hal+json;charset=UTF-8                                    |
    // | Content        | {                                                                     |
    // |                |   "id": <id>                                                          |
    // |                |   "firstName": "<first name>"                                         |
    // |                |   "lastName": "<last name>"                                           |
    // |                |   "links": {                                                          |
    // |                |     "self": { "href": "<host>users/<id>" }                            |
    // |                |     "edit": { "href": "<host>users/<id>" }                            |
    // |                |     "getProperty": { "href": "<host>users/<id>/property?name={name}" }|
    // |                |   }                                                                   |
    // |                | }                                                                     |
    // | Header         | X-Watermark:User-<id>                                                 |
    //
    // 注意禁止硬编码 URI
    // <--start-
    @GetMapping("/{id}")
    public ResponseEntity<User> getUser(@PathVariable Long id) {
        User user = new User(2L, "O_+", "^_^");

        Link selfRel = linkTo(methodOn(UserController.class).getUser(id)).withSelfRel();
        Link edit = linkTo(methodOn(UserController.class).updateUser(id,user)).withRel("edit");
        Link link = linkTo(methodOn(UserController.class).getUserProperty(2L,null)).withRel("getProperty");

        user.add(selfRel);
        user.add(edit);
        user.add(link);

        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Watermark","User-"+id);
        return ResponseEntity.ok().headers(headers).body(user);
    }
    // --end->

    @PutMapping("/{id}")
    public ResponseEntity updateUser(@PathVariable Long id, @RequestBody User user) {
        // 假装我们已经更新了用户:-D
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}/property")
    public Property getUserProperty(@PathVariable Long id, @RequestParam String name) {
        // 假装我们可以获得用户的一些属性:-D
        return new Property(name, String.format("User[%d].%s", id, name));
    }
}
