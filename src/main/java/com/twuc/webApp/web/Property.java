package com.twuc.webApp.web;

import org.springframework.hateoas.ResourceSupport;

public class Property extends ResourceSupport {
    private String name;
    private String value;

    public Property(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
